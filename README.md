## 6 hour and 24 hour timer on Arduino

I made this for turning off [this polisher](https://www.thingiverse.com/thing:3666116) at the correct time.

Details:

- I used Arduino Nano
- I wired my relay to output pin D3
- for 6 hour timer, bridge pins D10 and D12
- for 24 hour timer, bridge pins D11 and D12
- timer starts when Arduino is powered on
- on Nano, D13 controls the built-in LED so I used that
- indicator LED displays a pattern. If it blinks multiple times, it's the 24 hour timer. Timer progress is shown as different brightnesses.

![photo](https://f.mazie.rocks/IMG_20210417_021530_1.jpg)



