int timer_hours;
// need to store hours separately
// because int only goes up to 32768
int hours_elapsed = 0;
int seconds_elapsed = 0;

void n_blinks(int n, int time_scale=1) {
    for (int i = 0; i < n; i++) {
        digitalWrite(13, HIGH);
        delay(time_scale*50);
        digitalWrite(13, LOW);
        delay(time_scale*50);
    }
    // remaining time till 1 second
    delay(time_scale*(1000-n*100));
}

void show_brightness(int brightness) {
    for (int i = 0; i < 500; i+=20) {
        digitalWrite(13, HIGH);
        delay(brightness);
        digitalWrite(13, LOW);
        delay(20-brightness);
    }
}

void setup() {
    // indicator light:
    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);
    // relay starts LOW (on):
    pinMode(3, OUTPUT);
    digitalWrite(3, LOW);
    // make pin 10 an additional GND pin
    pinMode(10, OUTPUT);
    digitalWrite(10, LOW);
    // make pin 11 an additional VCC pin
    pinMode(11, OUTPUT);
    digitalWrite(11, HIGH);
    delay(10);
    // check which is connected to pin 12
    pinMode(12, INPUT);
    if (digitalRead(12)) {
        timer_hours = 24;
    } else {
        timer_hours = 6;
    }
}

void loop() {
    if (hours_elapsed < timer_hours) {
        n_blinks(timer_hours/6);
        delay(1000);
        // display progress
        show_brightness(hours_elapsed*20/timer_hours);
        show_brightness(20);
        // time passes
        seconds_elapsed += 3;
        if (seconds_elapsed > 3600){
            // the hours just fly by
            seconds_elapsed -= 3600;
            hours_elapsed++;
        }
    }
    else {
        // done
        digitalWrite(3, HIGH);
        // slow blink
        n_blinks(timer_hours/6, 4);
    }
}
